var RunLayer = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
		this.show();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 
		
		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		
		//氢氧化钠NaOH
		this.naoh = new NaOH(this);
		//试管TestTube
		this.testTube = new TestTube(this);
		//蛋白质（蛋清）Protein
		this.protein = new Protein(this);
		//油脂（食用油）Oil
		this.oil = new Oil(this);
		
		// NaOH塞子
		var NaOH_lid = new Button(this, 9, TAG_NAOH_LID, "#NaOH_lid.png", this.callback);
		NaOH_lid.setPosition(1140,464);
		NaOH_lid.setScale(0.9);
	},
	show:function(){
		var show=this.getChildByTag(TAG_SHOW);
},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},	
	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		   case TAG_NAOH_LID:
			   if(action==ACTION_DO1){
				   var ber = cc.bezierBy(1.5,[cc.p(-60,120),cc.p(-70,-80),cc.p(-70,-95)]);
				   var rotate = cc.rotateTo(1.5, -180);
				   var spawn = cc.spawn(ber, rotate);
					var seq = cc.sequence(spawn,cc.delayTime(0.5),this.callNext);
					p.runAction(seq);
			   }else if(action==ACTION_DO2){
				   var NaOH=this.getChildByTag(TAG_NAOH).getChildByTag(TAG_NAOH_BOTTLE);
				   var ber = cc.bezierBy(1.5,[cc.p(60,150),cc.p(80,150),cc.p(70,95)]);
				   var rotate = cc.rotateTo(1.5, 0);
				   var spawn = cc.spawn(ber, rotate);
				   var seq = cc.sequence(cc.callFunc(function() {
					   var show=new ShowTip("注意：使用完毕，试剂瓶标签向外",cc.p(1050,250));
				   }, this),spawn,cc.callFunc(function() {
//					   this.removeFromParent();
					   this.getChildByTag(TAG_NAOH).bottle_turnback(NaOH);
				   }, this),cc.delayTime(1),this.callNext);
				   p.runAction(seq);  
			   }
				break;
		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
			case TAG_PAPER:
				
				break;
			case TAG_SPOON:
				
				break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});