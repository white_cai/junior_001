var RunMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
    ctor:function () {
        this._super();
        this.loadBlackbord();
        this.loadTip();
        this.loadRun();
        this.loadTool();
        this.loadFlow(); 
        
        var self = this;  
        return true;
    },
    loadBlackbord:function(){
    	var bord=new cc.Sprite("#blackbord.png");
    	this.addChild(bord);
    	bord.setPosition(cc.p(640,595));
    },
    loadTip:function(){
    	ll.tip = new TipLayer(this);
    },
    loadTool:function(){
    	ll.tool = new ToolLayer(this);
    },
    loadRun:function(){
    	ll.run = new RunLayer(this);
    },
    loadFlow:function(){
    	gg.flow.setMain(this);
    	gg.flow.start();
    },
    over: function (){
    	ll.tip.over();
    	this.scheduleOnce(function(){
    		$.runScene(new FinishScene());
    	},2);
    	// 提交成绩
    	net.saveScore();
    },
});
