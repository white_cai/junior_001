/**

 * 试管

 */
TestTube= Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this, 1000);
		this.init();
		this.setTag(TAG_TESTTUBE);
	},
	init:function(){
		this.setCascadeOpacityEnabled(true);
		// 试管架（里层）
		var Rack1 = new Button(this, 2, TAG_RACK1, "#rack1.png", this.callback);
		Rack1.setPosition(210,340);
		Rack1.setScale(1.2);
		// 试管架（外层）
		var Rack2 = new Button(this, 11, TAG_RACK2, "#rack2.png", this.callback);
		Rack2.setPosition(210,340);
		Rack2.setScale(1.2);

		// 1号试管  手（里层）
		var hand_left1=new cc.Sprite("#hand_left.png");
		this.addChild(hand_left1,1,TAG_LEFT1);
		hand_left1.setPosition(cc.p(114,418));
		hand_left1.setVisible(false);
		// 1号试管  手（外层）
		var hand_left_part1=new cc.Sprite("#hand_left_part.png");
		this.addChild(hand_left_part1,11,TAG_LEFT_PART1);
		hand_left_part1.setPosition(cc.p(114,418));
		hand_left_part1.setVisible(false);	
		// 2号试管  手（里层）
		var hand_left2=new cc.Sprite("#hand_left.png");
		this.addChild(hand_left2,1,TAG_LEFT2);
		hand_left2.setPosition(cc.p(207,418));
		hand_left2.setVisible(false);
		// 2号试管  手（外层）
		var hand_left_part2=new cc.Sprite("#hand_left_part.png");
		this.addChild(hand_left_part2,13,TAG_LEFT_PART2);
		hand_left_part2.setPosition(cc.p(207,418));
		hand_left_part2.setVisible(false);
		
		// 试管1 
		var TestTube1 = new Button(this, 10, TAG_TESTTUBE1, "#testTube1.png", this.callback);
		TestTube1.setPosition(141,395);		
		// 试管2 
		var TestTube2 = new Button(this, 7, TAG_TESTTUBE2, "#testTube2.png", this.callback);
		TestTube2.setPosition(234,395);
		
		//试管1沉淀1
		var deposit1 = new Button(TestTube1, 10, TAG_DEPOSIT1, "#deposit1.png", this.callback);
		deposit1.setPosition(14,134);	
		deposit1.setOpacity(0);
		//试管1沉淀2
		var deposit2 = new Button(TestTube1, 10, TAG_DEPOSIT2, "#deposit2.png", this.callback);
		deposit2.setPosition(15,159);	
		deposit2.setOpacity(0);
		//试管2油脂
		var oil_line=new cc.Sprite("#cyl.png");
		oil_line.setPosition(cc.p(14,51));
		TestTube2.addChild(oil_line, 5, TAG_TESTTUBE2_OIL);
		oil_line.setColor(cc.color(244,225,69));
		oil_line.setScale(1.7, 1);
		oil_line.setOpacity(0);
		
		// 试管1水位线(
		var TestTube1_line=new cc.Sprite("#cyl.png");
		TestTube1_line.setPosition(cc.p(14,3));
		TestTube1.addChild(TestTube1_line, 5, TAG_TESTTUBE1_LINE);
		TestTube1_line.setVisible(false);
		// 试管2水位线  
		var TestTube2_line=new cc.Sprite("#cyl.png");
		TestTube2_line.setPosition(cc.p(14,3));
		TestTube2.addChild(TestTube2_line, 5, TAG_TESTTUBE2_LINE);
		TestTube2_line.setVisible(false);
		
//		// 试管1  ,水位线
//		var TestTube1_hand_line=new cc.Sprite("#cyl.png");
//		TestTube1_hand_line.setVisible(false);
//		TestTube1_hand_line.setPosition(cc.p(153,30));
//		TestTube1_hand.addChild(TestTube1_hand_line, 5, TAG_TESTTUBE1_HAND_LINE);
//		// 试管2  ,水位线
//		var TestTube2_hand_line=new cc.Sprite("#cyl.png");
//		TestTube2_hand_line.setVisible(false);
//		TestTube2_hand_line.setPosition(cc.p(153,30));
//		TestTube2_hand.addChild(TestTube2_hand_line, 5, TAG_TESTTUBE2_HAND_LINE);
		
//		// 试管1  手
//		var TestTube1_hand = new Button(this, 10, TAG_TESTTUBE1_HAND, "#hand1.png", this.callback);
//		TestTube1_hand.setPosition(114,390);
//		TestTube1_hand.setVisible(false);
//		// 试管2 手
//		var TestTube2_hand = new Button(this, 10, TAG_TESTTUBE2_HAND, "#hand2.png", this.callback);
//		TestTube2_hand.setPosition(206,390);
//		TestTube2_hand.setVisible(false);		

		//左手拿两根试管
		var hand_left = new Button(this, 1, TAG_LEFT, "#hand_left.png", this.callback);
		hand_left.setPosition(cc.p(550,250));
		hand_left.setVisible(false);
		var hand_left_part = new Button(this, 11, TAG_LEFT_PART, "#hand_left_part.png", this.callback);
//		hand_left_part.setPosition(cc.p(127,159));
		hand_left_part.setPosition(cc.p(550,250));
		hand_left_part.setVisible(false);

		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
	},
	loadflow:function(){
		var flow=new cc.Sprite("#cyl.png");
		flow.setScale(0.5,1);
		flow.setRotation(-45);
		flow.setPosition(cc.p(702,369));
		this.addChild(flow,5);
		var move1=cc.moveTo(1.2, cc.p(506,173));
		var fadeout=cc.fadeOut(1.2);
		var spa=cc.spawn(move1,fadeout);
		flow.runAction(cc.sequence(spa,cc.callFunc(function() {
			flow.removeFromParent();
		}, this)));		
	},
	move1:function(p){
		var move1=cc.moveTo(0.6,cc.p(114,588));
		var move2=cc.moveTo(0.6,cc.p(571,301));
		var move3=cc.moveTo(0.6,cc.p(591,311));
		var rota1=cc.rotateTo(0.6, 45);
		var spa1=cc.spawn(move3,rota1);
		var rota2=cc.rotateTo(0.6, 0);
		var spa2=cc.spawn(move2,rota2);
		var move4=cc.moveTo(0.6, cc.p(114,418));
		var seq=cc.sequence(move1,move2,spa1,cc.delayTime(4.2),spa2,move1,move4,cc.callFunc(function() {
			p.removeFromParent();
		}, this));
		p.runAction(seq);
	},
	move2:function(p){
		var move1=cc.moveTo(0.6,cc.p(207,588));
		var move2=cc.moveTo(0.6,cc.p(571,301));
		var move3=cc.moveTo(0.6,cc.p(591,311));
		var rota1=cc.rotateTo(0.6, 45);
		var spa1=cc.spawn(move3,rota1);
		var rota2=cc.rotateTo(0.6, 0);
		var spa2=cc.spawn(move2,rota2);
		var move4=cc.moveTo(0.6, cc.p(207,418));
		var seq=cc.sequence(move1,move2,spa1,cc.delayTime(4.2),spa2,move1,move4,cc.callFunc(function() {
			p.removeFromParent();
		}, this));
		p.runAction(seq);
	},
	move3:function(p){
		var move1=cc.moveTo(0.6, cc.p(173,654));
		var move2=cc.moveTo(0.6, cc.p(586,586));
		var move3=cc.moveTo(0.6, cc.p(586,335));
		var seq=cc.sequence(move1,move2,move3,cc.callFunc(function() {
			p.removeFromParent();
		}, this));
		p.runAction(seq);
	},
	move4:function(p){
		var move1=cc.moveTo(0.6, cc.p(270,654));
		var move2=cc.moveTo(0.6, cc.p(619,587));
		var move3=cc.moveTo(0.6, cc.p(619,335));
		var seq=cc.sequence(move1,move2,move3,cc.callFunc(function() {
			p.removeFromParent();			
			var rack1=this.getChildByTag(TAG_RACK1);
			var rack2=this.getChildByTag(TAG_RACK2);
			if(rack1!=null){
				rack1.removeFromParent();
				rack2.removeFromParent();
				}
			}, this));
		p.runAction(seq);
	},
	callback:function (p){
		var action=gg.flow.flow.action;
		var func=cc.callFunc(this.actionDone,this);
		var hand=this.getParent().getChildByTag(TAG_NAOH).getChildByTag(TAG_RIGHT);
		var part=this.getParent().getChildByTag(TAG_NAOH).getChildByTag(TAG_RIGHT_PART);
		switch(p.getTag()){
		case TAG_TESTTUBE1:
			if(action==ACTION_DO1){
				var left=this.getChildByTag(TAG_LEFT1);
				left.setVisible(true);
				this.move1(left);
				var left_part=this.getChildByTag(TAG_LEFT_PART1);
				left_part.setVisible(true);
				this.move1(left_part);

				var move=cc.moveTo(0.6, cc.p(141,395));
				var move1=cc.moveTo(0.6, cc.p(141,565));
				var move2=cc.moveTo(0.6, cc.p(598,278));
				var rota1=cc.rotateTo(0.6, 45);
				var rota2=cc.rotateTo(0.6, 0);
				var seq=cc.sequence(cc.callFunc(function() {

				}, this),move1,move2,rota1,cc.callFunc(function() {
					var show=new ShowTip("注意：试管溶液不超过三分之一" ,cc.p(810,220));
					hand.runAction(cc.spawn(cc.rotateTo(0.6, -60),cc.moveTo(0.6, cc.p(1191,412))));				
					part.runAction(cc.spawn(cc.rotateTo(0.6, -60),cc.moveTo(0.6, cc.p(1191,412))));
					var NaOH=this.getParent().getChildByTag(TAG_NAOH).getChildByTag(TAG_NAOH_BOTTLE);
					NaOH.runAction(cc.sequence(cc.rotateTo(0.6,-60),cc.callFunc(function() {
						this.schedule(this.loadflow, 0.01,80);
						var line=this.getChildByTag(TAG_TESTTUBE1).getChildByTag(TAG_TESTTUBE1_LINE);
						var move1=cc.moveTo(0.4, cc.p(14,17));
						var rota1=cc.rotateTo(0.4, -45);
						var sca1=cc.scaleTo(0.4,1.9,1);
						var spa1=cc.spawn(rota1,sca1,move1);

						var mov=cc.moveTo(1, cc.p(14,50));						
						var rota2=cc.rotateTo(0.6, 0);
						var sca2=cc.scaleTo(0.6,1.7,1);
						var spa2=cc.spawn(rota2,sca2);
						var seq=cc.sequence(cc.delayTime(1.2),cc.callFunc(function() {
							line.setVisible(true);
						}, this),spa1,mov,cc.delayTime(1),spa2);
						line.runAction(seq);
					}, this),cc.delayTime(2.6),cc.callFunc(function() {
						hand.runAction(cc.spawn(cc.rotateTo(0.6, 0),cc.moveTo(0.6, cc.p(1175,362))));
						part.runAction(cc.spawn(cc.rotateTo(0.6, 0),cc.moveTo(0.6, cc.p(1175,362))));
					}, this),cc.rotateTo(0.6,0)));
				}, this),cc.delayTime(4.2),rota2,move1,move,func);
				p.runAction(seq);
			}else if(action==ACTION_DO2){
				var hand_right=new cc.Sprite("#hand_right.png");
				this.addChild(hand_right,9);
				hand_right.setPosition(cc.p(173,483));
				var hand_right_part=new cc.Sprite("#hand_right_part.png");
				this.addChild(hand_right_part,11);
				hand_right_part.setPosition(cc.p(173,483));

				var move1=cc.moveTo(0.6, cc.p(141,570));
				var move2=cc.moveTo(0.6, cc.p(550,500));
				var move3=cc.moveTo(0.6, cc.p(550,250));
				var seq=cc.sequence(move1,move2,cc.callFunc(function() {
					var hand_left=this.getChildByTag(TAG_LEFT);
					hand_left.setVisible(true);
					var hand_left_part=this.getChildByTag(TAG_LEFT_PART);
					hand_left_part.setVisible(true);
				}, this),move3,cc.callFunc(function() {
					var hand_left=this.getChildByTag(TAG_LEFT);
					p.removeFromParent();
					hand_left.addChild(p,10);	
					p.setPosition(cc.p(125,159));
					
				}, this),func);
				p.runAction(seq);

				this.move3(hand_right);
				this.move3(hand_right_part);
			}
			break;
		case TAG_TESTTUBE2:
			if(action==ACTION_DO1){
				var left=this.getChildByTag(TAG_LEFT2);
				left.setVisible(true);
				this.move2(left);
				var left_part=this.getChildByTag(TAG_LEFT_PART2);
				left_part.setVisible(true);
				this.move2(left_part);

				var move=cc.moveTo(0.6, cc.p(234,395));
				var move1=cc.moveTo(0.6, cc.p(234,565));
				var move2=cc.moveTo(0.6, cc.p(598,278));
				var rota1=cc.rotateTo(0.6, 45);
				var rota2=cc.rotateTo(0.6, 0);
				var seq=cc.sequence(move1,move2,rota1,cc.callFunc(function() {
					hand.runAction(cc.spawn(cc.rotateTo(0.6, -60),cc.moveTo(0.6, cc.p(1188,412))));				
					part.runAction(cc.spawn(cc.rotateTo(0.6, -60),cc.moveTo(0.6, cc.p(1188,412))));
					var layer=this.getParent().getChildByTag(TAG_NAOH);
					var NaOH=this.getParent().getChildByTag(TAG_NAOH).getChildByTag(TAG_NAOH_BOTTLE);
					NaOH.runAction(cc.sequence(cc.rotateTo(0.6,-60),cc.callFunc(function() {
						this.schedule(this.loadflow, 0.01,80);
						var line=this.getChildByTag(TAG_TESTTUBE2).getChildByTag(TAG_TESTTUBE2_LINE);
						var move1=cc.moveTo(0.4, cc.p(14,17));
						var rota1=cc.rotateTo(0.4, -45);
						var sca1=cc.scaleTo(0.4,1.9,1);
						var spa1=cc.spawn(rota1,sca1,move1);

						var mov=cc.moveTo(1, cc.p(14,50));						
						var rota2=cc.rotateTo(0.6, 0);
						var sca2=cc.scaleTo(0.6,1.7,1);
						var spa2=cc.spawn(rota2,sca2);
						var seq=cc.sequence(cc.delayTime(1.2),cc.callFunc(function() {
							line.setVisible(true);
						}, this),spa1,mov,cc.delayTime(1),spa2);
						line.runAction(seq);
					}, this),cc.delayTime(2.6),cc.callFunc(function() {
						hand.runAction(cc.spawn(cc.rotateTo(0.6, 0),cc.moveTo(0.6, cc.p(1175,362))));
						part.runAction(cc.spawn(cc.rotateTo(0.6, 0),cc.moveTo(0.6, cc.p(1175,362))));
					}, this),cc.rotateTo(0.6,0),cc.callFunc(function() {
						layer.runAction(cc.sequence(cc.delayTime(0.5),cc.moveTo(0.6,cc.p(0,0)),cc.callFunc(function() {
							hand.removeFromParent();
							part.removeFromParent();
						}, this)));						
					}, this)));
				}, this),cc.delayTime(4.2),rota2,move1,move,func);
				p.runAction(seq);
			}else if(action==ACTION_DO2){
				var hand_right=new cc.Sprite("#hand_right.png");
				this.addChild(hand_right,6);
				hand_right.setPosition(cc.p(270,483));
				var hand_right_part=new cc.Sprite("#hand_right_part.png");
				this.addChild(hand_right_part,11);
				hand_right_part.setPosition(cc.p(270,483));

				var move1=cc.moveTo(0.6, cc.p(234,570));
				var move2=cc.moveTo(0.6, cc.p(585,500));
				var move3=cc.moveTo(0.6, cc.p(585,250));
				var seq=cc.sequence(move1,move2,move3,cc.callFunc(function() {
					var hand_left=this.getChildByTag(TAG_LEFT);
					p.removeFromParent();
					hand_left.addChild(p,10);
					p.setPosition(cc.p(160,159));
				}, this),func);
				p.runAction(seq);

				this.move4(hand_right);
				this.move4(hand_right_part);
			}
			break;
		case TAG_LEFT_PART:
			var hand=this.getParent().getChildByTag(TAG_TESTTUBE).getChildByTag(TAG_LEFT);
			var rot1=cc.rotateTo(0.15, 15);
			var rot2=cc.rotateTo(0.15, 0);
			var seq=cc.sequence(rot1,rot2);
			var act=cc.repeat(seq, 6);
			var act1=act.clone();
			var se=cc.sequence(act1,cc.rotateTo(0.15, 0));
			p.runAction(se);
			
			hand.runAction(cc.sequence(cc.callFunc(function() {
				var testtube_hand=this.getParent().getChildByTag(TAG_TESTTUBE).getChildByTag(TAG_LEFT);;
				var deposit1=testtube_hand.getChildByTag(TAG_TESTTUBE1).getChildByTag(TAG_DEPOSIT1);
				var deposit2=testtube_hand.getChildByTag(TAG_TESTTUBE1).getChildByTag(TAG_DEPOSIT2);
				var oil_line=testtube_hand.getChildByTag(TAG_TESTTUBE2).getChildByTag(TAG_TESTTUBE2_OIL);
				var mov1=cc.moveTo(0.15,cc.p(14,134));
				var mov2=cc.moveTo(0.15, cc.p(14,115));
				var mov3=cc.moveTo(2, cc.p(14,115));
				var seqqq=cc.sequence(mov1,mov2);
				var acttt=cc.repeat(seqqq,6);
				deposit1.runAction(cc.sequence(acttt,cc.delayTime(0.4),mov3));
				oil_line.runAction(cc.fadeOut(2));
			}, this),act,rot2,cc.callFunc(function() {
				var show=new ShowTip("观察实验结果：\n1号试管白色絮状沉淀\n" +
						"2号试管油脂皂化反应，水解" ,cc.p(850,250));
			}, this),cc.delayTime(5),this.callNext));			
			break;
		default:
			break;
		}
	},
	actionDone:function(p){
		switch (p.getTag()) {
		case TAG_TESTTUBE1:			
			break;
		default:
			break;
		}
		gg.flow.next();
	}
})
