/**
 * 氢氧化钠NaOH溶液试剂瓶
 */
NaOH= Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this, 51);
		this.init();
		this.setTag(TAG_NAOH);
	},
	init:function(){
		this.setCascadeOpacityEnabled(true);
		
		var hand = new Button(this, 9, TAG_RIGHT, "#hand_right.png", this.callback);
		hand.setPosition(cc.p(1175,362));
		hand.setVisible(false);
		var part = new Button(this, 11, TAG_RIGHT_PART, "#hand_right_part.png", this.callback);
		part.setPosition(cc.p(1175,362));
		part.setVisible(false);
		// NaOH瓶子
		this.body = new Button(this, 10, TAG_NAOH_BOTTLE, "#NaOH/NaOH_bottle_1.png", this.callback);
		this.body.setPosition(1140,400);
		this.body.setScale(1.5);
	
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
	},
	bottle_turn:function(p){
		var animFrames = [];
		for (var i = 2; i < 6; i++) {
			var str = "NaOH/NaOH_bottle_" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.15,1);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},
	bottle_turnback:function(){
		var bottle=this.getChildByTag(TAG_NAOH_BOTTLE);
		var animFrames = [];
		for (var i = 5; i >0; i--) {
			var str = "NaOH/NaOH_bottle_" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.15,1);
		var action = cc.animate(animation);
		var seq=cc.sequence(action,cc.callFunc(function() {
		}, this));
		bottle.runAction(seq);
	},
	callback:function (p){
		var action=gg.flow.flow.action;
		var hand=this.getChildByTag(TAG_RIGHT);
		var part=this.getChildByTag(TAG_RIGHT_PART);
		var func=cc.callFunc(this.actionDone,this);
		switch(p.getTag()){
		case TAG_NAOH_BOTTLE:
			this.bottle_turn(p);
	
			var move=cc.moveTo(0.6, cc.p(-380,-45));
			var seq=cc.sequence(cc.delayTime(0.6),cc.callFunc(function() {
				hand.setVisible(true);
				part.setVisible(true);	
			}, this),move,cc.callFunc(function() {
				var show=new ShowTip("注意：标准拿法，标签向手心",cc.p(1000,200));
			}, this),this.callNext);
			this.runAction(seq);
			break;
		default:
			break;
		}
	},
	actionDone:function(p){
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_NAOH_BOTTLE:

			break;
		default:
			break;
		}
		gg.flow.next();
	}
})