/**
 * 油脂（食用油）
 */
Oil= Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this, 49);
		this.init();
		this.setTag(TAG_OIL);
	},
	init:function(){
		this.setCascadeOpacityEnabled(true);
		
		var hand = new Button(this, 8, TAG_RIGHT, "#hand_right.png", this.callback);
		hand.setPosition(cc.p(1045,480));
		hand.setVisible(false);
		var part = new Button(this, 11, TAG_RIGHT_PART, "#hand_right_part.png", this.callback);
		part.setPosition(cc.p(1045,480));
		part.setVisible(false);
		
		// 食用油滴瓶
		var Oil_bottle = new Button(this, 10, TAG_OIL_BOTTLE, "#bottle1.png", this.callback);
		Oil_bottle.setPosition(1010,400);
		Oil_bottle.setScale(1.5);
		// 食用油滴管
		var Oil_drop = new Button(this, 9, TAG_OIL_DROP, "#dropper3.png", this.callback);
		Oil_drop.setPosition(1010,448);
		Oil_drop.setScale(1.5);
		//食用油标签
		var Oil_label = new cc.LabelTTF("食用油",gg.fontName,12);
		Oil_label.setPosition(21,31);
		Oil_label.setColor(cc.color(0, 0, 0));
		Oil_bottle.addChild(Oil_label, 11);

		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
	},

	loadpoint:function(){
		var point=new cc.Sprite("#wpoint.png");
		point.setPosition(cc.p(584,403));
		point.setColor(cc.color(244,225,69));
		this.addChild(point,5);
		var move1=cc.moveTo(0.8, cc.p(584,167));
		var fadeout=cc.fadeOut(0);
		var seq=cc.sequence(move1,fadeout,cc.callFunc(function() {
			point.removeFromParent();
		}, this));
		point.runAction(seq);	
	},
	move:function(p){
		var move1=cc.moveTo(0.6, cc.p(1045,590));
		var move2=cc.moveTo(0.6, cc.p(619,537));
		var move3=cc.moveTo(0.6, cc.p(1045,480));
		var seq=cc.sequence(move1,move2,cc.delayTime(2.5),move1,move3,cc.callFunc(function() {
			p.removeFromParent();
		},this));
		p.runAction(seq);
	},
	callback:function (p){
		var action=gg.flow.flow.action;
		var func=cc.callFunc(this.actionDone,this);
		var hand=this.getChildByTag(TAG_RIGHT);
		var part=this.getChildByTag(TAG_RIGHT_PART);
		switch(p.getTag()){
		case TAG_OIL_DROP:
			var move1=cc.moveTo(0.6, cc.p(1010,558));
			var move2=cc.moveTo(0.6, cc.p(584,505));
			var move3=cc.moveTo(0.6, cc.p(1010,448));
			var seq=cc.sequence(cc.callFunc(function() {
				hand.setVisible(true);
				part.setVisible(true);
			}, this),move1,move2,cc.callFunc(function() {
				this.schedule(this.loadpoint, 0.8,1);
				var testtube=this.getParent().getChildByTag(TAG_TESTTUBE);
				var oil_line=testtube.getChildByTag(TAG_LEFT).getChildByTag(TAG_TESTTUBE2).getChildByTag(TAG_TESTTUBE2_OIL);
				oil_line.runAction(cc.sequence(cc.delayTime(1.5),cc.fadeIn(0.1),cc.delayTime(0.8),cc.scaleTo(0.1,1.7,2)));
			}, this),cc.delayTime(2.5),move1,move3,func);
			p.runAction(seq);
			
			this.move(hand);
			this.move(part);
			break;
		default:
			break;
		}
	},
	actionDone:function(p){
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_OIL_DROP:
			break;
		default:
			break;
		}
		gg.flow.next();
	}
})