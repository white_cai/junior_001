/**
 * 蛋白质（蛋清）
 */
Protein= Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this, 50);
		this.init();
		this.setTag(TAG_PROTEIN);
	},
	init:function(){
		this.setCascadeOpacityEnabled(true);
		
		var hand = new Button(this, 8, TAG_RIGHT, "#hand_right.png", this.callback);
		hand.setPosition(cc.p(970,480));
		hand.setVisible(false);
		var part = new Button(this, 11, TAG_RIGHT_PART, "#hand_right_part.png", this.callback);
		part.setPosition(cc.p(970,480));
		part.setVisible(false);
	
		// 蛋清瓶子
		var Protein_bottle = new Button(this, 10, TAG_PROTEIN_BOTTLE, "#bottle.png", this.callback);
		Protein_bottle.setPosition(935,400);
		Protein_bottle.setScale(1.5);
		// 蛋清滴管
		var Protein_drop = new Button(this, 9, TAG_PROTEIN_DROP, "#dropper2.png", this.callback);
		Protein_drop.setPosition(935,448);
		Protein_drop.setScale(1.5);
		//蛋清标签
		var Protein_label = new cc.LabelTTF("蛋清",gg.fontName,12);
		Protein_label.setPosition(21,31);
		Protein_label.setColor(cc.color(0, 0, 0));
		Protein_bottle.addChild(Protein_label, 11);
		
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callNext.retain();
	},
	loadpoint:function(){
		var point=new cc.Sprite("#wpoint.png");
		point.setPosition(cc.p(550,403));
		this.addChild(point,5);
		var move1=cc.moveTo(0.8, cc.p(550,167));
		var fadeout=cc.fadeOut(0);
		var seq=cc.sequence(move1,fadeout,cc.callFunc(function() {
			point.removeFromParent();
		}, this));
		point.runAction(seq);	
	},
move:function(p){
	var move1=cc.moveTo(0.6, cc.p(970,590));
	var move2=cc.moveTo(0.6, cc.p(584,537));
	var move3=cc.moveTo(0.6, cc.p(970,480));
	var seq=cc.sequence(move1,move2,cc.delayTime(2.5),move1,move3,cc.callFunc(function() {
		p.removeFromParent();
	},this));
	p.runAction(seq);
},
	callback:function (p){
		var action=gg.flow.flow.action;
		var func=cc.callFunc(this.actionDone,this);
		var hand=this.getChildByTag(TAG_RIGHT);				
		var part=this.getChildByTag(TAG_RIGHT_PART);
		switch(p.getTag()){
		case TAG_PROTEIN_DROP:
			var move1=cc.moveTo(0.6, cc.p(935,558));
			var move2=cc.moveTo(0.6, cc.p(549,505));
			var move3=cc.moveTo(0.6, cc.p(935,448));
			var seq=cc.sequence(cc.callFunc(function() {
				hand.setVisible(true);
				part.setVisible(true);
			}, this),move1,move2,cc.callFunc(function() {
				this.schedule(this.loadpoint, 0.8,1);
				var testtube=this.getParent().getChildByTag(TAG_TESTTUBE);
				var deposit1=testtube.getChildByTag(TAG_LEFT).getChildByTag(TAG_TESTTUBE1).getChildByTag(TAG_DEPOSIT1);
				deposit1.runAction(cc.sequence(cc.delayTime(1.5),cc.fadeIn(1.6)));
				var deposit2=testtube.getChildByTag(TAG_LEFT).getChildByTag(TAG_TESTTUBE1).getChildByTag(TAG_DEPOSIT2);
				var spa=cc.spawn(cc.fadeIn(1.6),cc.moveTo(1.6,cc.p(15,141)));
				deposit2.runAction(cc.sequence(cc.delayTime(1.5),spa));
				var line=testtube.getChildByTag(TAG_LEFT).getChildByTag(TAG_TESTTUBE1).getChildByTag(TAG_TESTTUBE1_LINE);
				line.runAction(cc.sequence(cc.delayTime(1.5),cc.moveTo(0.2,cc.p(14,51)),cc.delayTime(0.8),cc.moveTo(0.2,cc.p(14,52))));
			}, this),cc.delayTime(2.5),move1,move3,func);
			p.runAction(seq);

			this.move(hand);
			this.move(part);
			break;
		default:
			break;
		}
	},
	actionDone:function(p){
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_PROTEIN_DROP:

			break;

		default:
			break;
		}
		gg.flow.next();
	}
})