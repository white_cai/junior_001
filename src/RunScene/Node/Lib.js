/**
 * 药品库
 */
Lib = cc.Sprite.extend({
	libArr: [],
	openFlag:false,
	doing:false,
	rm : 5,
	ctor:function(p){
		this._super("#lib_back.png");
		p.addChild(this,30);
		this.setTag(TAG_LIB);
		this.init();
	},
	init:function(){
		this.libArr = [];
		this.openFlag = false;
		this.setPosition(gg.width + this.width * 0.5 + 9,550 + 30);

//		var salt = new LibButton(this, 10, TAG_LIB_SALT,"#salt3.png", this.callback);
//		salt.setPosition(30, this.height * 0.5);
//
//		var cyl = new LibButton(this, 10, TAG_LIB_CYLINDER,"#cylinder.png", this.callback);
//		cyl.right(salt, this.rm);
//		
//		var kettle = new LibButton(this, 10, TAG_LIB_KETTLE,"#kettle.png", this.callback);
//		kettle.right(cyl, this.rm);
//		
//		var rod =  new LibButton(this, 10, TAG_LIB_ROD,"#rod.png", this.callback);
//		rod.right(kettle, this.rm);
//		
//		var beaker = new LibButton(this, 10, TAG_LIB_BEAKER,"#beaker/beaker.png", this.callback);
//		beaker.right(rod, this.rm);
//
//		var ironShelf = new LibButton(this, 10, TAG_LIB_IRON,"#ironShelf.png", this.callback);
//		ironShelf.right(beaker, this.rm);
//
//		var funnel = new LibButton(this, 10, TAG_LIB_FUNNEL,"#funnel.png", this.callback);
//		funnel.right(ironShelf, this.rm);
//		
//		var paper =  new LibButton(this, 10, TAG_LIB_PAPER,"#filter_paper2.png", this.callback);
//		paper.right(funnel, this.rm);
//
//		var shelf = new LibButton(this, 10, TAG_LIB_SHELF,"#shelf.png", this.callback);
//		shelf.right(paper, this.rm);
//
//		var net = new LibButton(this, 10, TAG_LIB_NET,"#asbestosNet.png", this.callback);
//		net.right(shelf, this.rm);
//		
//		var drop = new LibButton(this, 10, TAG_LIB_DROP,"#drop_bottle.png", this.callback);
//		drop.right(net, this.rm);
//		
//		var lamp = new LibButton(this, 10, TAG_LIB_LAMP,"#lamp/lp1.png", this.callback);
//		lamp.right(drop, this.rm);
//		
//		var match = new LibButton(this, 10, TAG_LIB_MATCH,"#lamp/match_box.png", this.callback);
//		match.right(lamp, this.rm);
//
//		var ph = new LibButton(this, 10, TAG_LIB_PH,"#color_card.png", this.callback);
//		ph.right(match, this.rm);
//		
//		var pot = new LibButton(this, 10, TAG_LIB_POT,"#pot.png", this.callback);
//		pot.right(ph, this.rm);
//		
//		var air = new LibButton(this, 10, TAG_LIB_AIR,"#air.png", this.callback);
//		air.right(pot, this.rm);
//		
//		var spoon = new LibButton(this, 10, TAG_LIB_SPOON,"#spoon3.png", this.callback);
//		spoon.right(air, this.rm);
//		
//		var paper2 = new LibButton(this, 10, TAG_LIB_PAPER2,"#paper.png", this.callback);
//		paper2.right(spoon, this.rm);
//		
//		var wideBottle = new LibButton(this, 10, TAG_LIB_WIDE,"#bottle/wide.png", this.callback);
//		wideBottle.right(paper2, this.rm);

	},
	moveLib:function(tag,width){
		width = 75;
		var begin = false;
		for(var i in this.libArr){
			var libTag = this.libArr[i];
			if(tag == libTag){
				begin = true;
			}
			if(begin){
				var lib = this.getChildByTag(libTag);
				if(lib != null){
					lib.runAction(cc.moveBy(0.5,cc.p(-width, 0)));
				}
			}
		}
	},
	callback:function(p){
		var pos = this.getPosition(); 
		var action = gg.flow.flow.action;
		switch(p.getTag()){
			case TAG_LIB_SALT:
				ll.run.loadSalt(pos);
				break;
			case TAG_LIB_CYLINDER:
				ll.run.loadCyl(pos);
				break;
			case TAG_LIB_KETTLE:
				ll.run.loadKettle(pos);
				break;
			case TAG_LIB_PAPER:
				ll.run.showPaper(pos)
				break;
			case TAG_LIB_IRON:
				ll.run.loadIron(pos);
				break;
			case TAG_LIB_BEAKER:
				ll.run.loadBeaker(pos);	
				break;
			case TAG_LIB_FUNNEL:
				ll.run.loadFunnel(pos);
				break;
			case TAG_LIB_ROD:
				ll.run.loadRod(pos);
				break;
			case TAG_LIB_LAMP:
				ll.run.loadLamp(pos);
				break;
			case TAG_LIB_MATCH:
				ll.run.loadMatch(pos);
				break;
			case TAG_LIB_SHELF:
				ll.run.loadShelf(pos);
				break;
			case TAG_LIB_NET:
				ll.run.loadNet(pos);
				break;
			case TAG_LIB_DROP:
				ll.run.loadDrop(pos);
				break;
			case TAG_LIB_POT:
				ll.run.loadPot(pos);
				break;
			case TAG_LIB_PH:
				ll.run.loadPH(pos);
				break;
			case TAG_LIB_AIR:
				ll.run.loadAir(pos);
				break;
			case TAG_LIB_SPOON:
				ll.run.loadSpoon(pos);
				break;
			case TAG_LIB_PAPER2:
				ll.run.loadPaper2(pos);
				break;
			case TAG_LIB_WIDE:
				ll.run.loadWide(pos);
			break;
			default:
				break;
		}
		if(action == ACTION_NONE){
			this.moveLib(p.getTag(), p.width * p.getScale());
			p.removeFromParent(true);
		}
	},
	isOpen:function(){
		return this.openFlag; 
	},
	open:function(){
		if(this.openFlag || this.doing){
			return;
		}
		this.doing = true;
		var move = cc.moveBy(0.4, cc.p(-this.width,0));
		var func = cc.callFunc(function(){
			this.openFlag = true;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			 if(tag instanceof Array){
				 if(TAG_LIB_MIN < tag[1]){
					 // 显示箭头
					 gg.flow.location();
				 }
			 }
		}, this);
		var seq = cc.sequence(move,func);
		this.runAction(seq);
	},
	close:function(){
		if(!this.openFlag || this.doing){
			return;
		}
		this.doing = true;
		var move = cc.moveBy(0.4, cc.p(this.width,0));
		var func = cc.callFunc(function(){
			this.openFlag = false;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 隐藏箭头
					ll.tip.arr.out();
				}
			}
		}, this);
		var seq = cc.sequence(move,func);
		this.runAction(seq);
	}
});
TAG_LIB_MIN = 30000;
TAG_LIB_SALT = 30001;
TAG_LIB_SPOON = 30002;
TAG_LIB_KETTLE = 30003;
TAG_LIB_PAPER = 30004;
TAG_LIB_IRON = 30005;
TAG_LIB_FUNNEL = 30006;
TAG_LIB_ROD = 30007;
TAG_LIB_BEAKER = 30008;
TAG_LIB_LAMP = 30009;
TAG_LIB_SHELF = 30010;
TAG_LIB_NET = 30011;
TAG_LIB_DROP = 30012;
TAG_LIB_CYLINDER = 30013;
TAG_LIB_POT = 30014;
TAG_LIB_PH = 30015;
TAG_LIB_AIR = 30016;
TAG_LIB_PAPER2 = 30017;
TAG_LIB_MATCH = 30018;
TAG_LIB_WIDE = 30019;
libRelArr = [
     {tag:TAG_LIB_MIN, name:""},
     {tag:TAG_LIB_SALT, name:"粗盐"},
     {tag:TAG_LIB_SPOON, name:"药匙"},
     {tag:TAG_LIB_KETTLE, name:"蒸馏水瓶"},
     {tag:TAG_LIB_PAPER, name:"折叠滤纸"},
     {tag:TAG_LIB_IRON, name:"铁架"},
     {tag:TAG_LIB_FUNNEL, name:"漏斗"},
     {tag:TAG_LIB_ROD, name:"玻璃棒"},
     {tag:TAG_LIB_BEAKER, name:"烧杯"},
     {tag:TAG_LIB_LAMP, name:"酒精灯"},
     {tag:TAG_LIB_SHELF, name:"三脚架"},
     {tag:TAG_LIB_NET, name:"石棉网"},
     {tag:TAG_LIB_DROP, name:"滴瓶"},
     {tag:TAG_LIB_CYLINDER, name:"量筒"},
     {tag:TAG_LIB_POT, name:"蒸发皿"},
     {tag:TAG_LIB_PH, name:"pH试纸"},
     {tag:TAG_LIB_AIR, name:"空气压缩机"},
     {tag:TAG_LIB_PAPER2, name:"滤纸"},
     {tag:TAG_LIB_MATCH, name:"火柴盒"},
     {tag:TAG_LIB_WIDE, name:"广口瓶"}];
