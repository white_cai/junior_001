var Button = Angel.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,zIndex,tag,normalImage, callback, back) {
		this._super(parent,normalImage, callback, back);
		this.setTag(tag);
		this.setLocalZOrder(zIndex);
		// 默认不能点击
		this.setEnable(false);
	},
	preCall:function(pos){
		// 隐藏箭头
		ll.tip.arr.out();
		// 删除提示
		this.deleteShow();
		this.setEnable(false);
		// 操作成功
		ll.tip.mdScore(10);
		if(!gg.errFlag){
			gg.oneSure ++;
		}
		gg.errFlag = false;
		_.clever();
	},
	exeUnEnable:function(pos){
		if(gg.errPos == null || (gg.errPos.x != pos.x || gg.errPos.y!=pos.y)){
			var othertag=gg.flow.flow.othertag;															
			for(var i in othertag){
				if(othertag[i]==this.getTag()){	
					if(this.getTag()==2061){
						if(pos.y > 444){
							return;
						}else{
							this.error();
						}
					}
					if(this.getTag()==2041){
						if((pos.x>920&&pos.x<950)&&pos.y > 363){
							return;
						}else{
							this.error();
						}
						
					}
					if(this.getTag()==2051){
						if((pos.x>995&&pos.x<1026)&&pos.y > 363){
							return;
						}else{
							this.error();
						}

					}
					return;
				}
			}			
			gg.errPos = pos;
			// 操作失败
			ll.tip.mdScore(-3);
			gg.errFlag = true;
			gg.errorStep ++;
			_.error();
		}
	},
	error:function(pos){
		gg.errPos = pos;
		// 操作失败
		ll.tip.mdScore(-3);
		gg.errFlag = true;
		gg.errorStep ++;
		_.error();
	},
	kill:function(){
		this.removeFromParent(true);
	},
	hiddleAndKill:/**
	 * @param dt
	 *            时间,默认0.5秒
	 */
		function(dt){
		if(dt == null){
			dt = 0.5;
		}
		this.runAction(cc.fadeOut(dt),cc.callFunc(function(){
			this.kill();
		}, this));
	},
	deleteShow:function(){
		if(ll.run == null){
			return;
		}
		var show = ll.run.getChildByTag(TAG_SHOW);
		if(show == null){
			return;
		}
		show.kill();
	}
})